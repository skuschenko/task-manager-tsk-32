package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "create new task";

    @NotNull
    private static final String NAME = "task-create";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("name");
        @NotNull final String name = TerminalUtil.nextLine();
        showParameterInfo("description");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        @Nullable final Task task = taskService.add(userId, name, description);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
