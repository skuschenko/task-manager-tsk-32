package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "update task by id";

    @NotNull
    private static final String NAME = "task-update-by-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("id");
        @NotNull final String valueId = TerminalUtil.nextLine();
        @NotNull final ITaskService taskService
                = serviceLocator.getTaskService();
        @Nullable Task task = taskService.findOneById(userId, valueId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showParameterInfo("name");
        @NotNull final String valueName = TerminalUtil.nextLine();
        showParameterInfo("description");
        @NotNull final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneById(
                userId, valueId, valueName, valueDescription
        );
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}