package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserLockByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "lock user by login";

    @NotNull
    private static final String NAME = "lock-user-by-login";

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
