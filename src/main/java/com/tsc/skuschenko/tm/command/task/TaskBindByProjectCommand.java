package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskBindByProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "bind task by project";

    @NotNull
    private static final String NAME = "task-bind-by-project";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        showParameterInfo("task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        @Nullable Task task = projectTaskService.bindTaskByProject(
                userId, projectId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}