package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "start project by index";

    @NotNull
    private static final String NAME = "project-start-by-index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("index");
        @NotNull final Integer value = TerminalUtil.nextNumber() - 1;
        @NotNull final IProjectService projectService
                = serviceLocator.getProjectService();
        @Nullable final Project project =
                projectService.startByIndex(userId, value);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
