package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Optional;

public final class AllCommandsShowCommand extends AbstractCommand {

    @NotNull
    private static final String DESCRIPTION = "commands";

    @NotNull
    private static final String NAME = "commands";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @NotNull final Collection<String> names =
                serviceLocator.getCommandService().getListCommandNames();
        names.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(System.out::println);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
