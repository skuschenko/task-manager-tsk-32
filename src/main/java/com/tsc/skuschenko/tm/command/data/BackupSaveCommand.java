package com.tsc.skuschenko.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tsc.skuschenko.tm.dto.Domain;
import com.tsc.skuschenko.tm.exception.empty.EmptyXmlPathException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Optional;

public final class BackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    private static final String DESCRIPTION = "backup data in xml file";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileBackupPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyXmlPathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(domain);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
