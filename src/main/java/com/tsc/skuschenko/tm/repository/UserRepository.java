package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRepository extends AbstractRepository<User>
        implements IUserRepository {

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return entities.stream()
                .filter(item -> email.equals(item.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return entities.stream()
                .filter(item -> login.equals(item.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        entities.remove(user);
        return user;
    }

}
