package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IBusinessRepository;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(@NotNull final String userId) {
        Optional.ofNullable(findAll(userId))
                .ifPresent(item -> item.forEach(entities::remove));
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<E> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<E> comparator
    ) {
        return entities.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findOneById(
            @NotNull final String userId, @NotNull final String id
    ) {
        return entities.stream()
                .filter(item -> id.equals(item.getId()) &&
                        userId.equals(item.getUserId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndex(
            @NotNull final String userId, @NotNull final Integer index
    ) {
        return entities.size() > index &&
                userId.equals(entities.get(index).getUserId()) ?
                entities.get(index) : null;
    }

    @Nullable
    @Override
    public E findOneByName(
            @NotNull final String userId, @NotNull final String name
    ) {
        return entities.stream()
                .filter(item -> name.equals(item.getId()) &&
                        userId.equals(item.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E removeOneById(
            @NotNull final String userId, @NotNull final String id
    ) {
        @Nullable final Optional<E> entity =
                Optional.ofNullable(findOneById(userId, id));
        entity.ifPresent(entities::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeOneByIndex(
            @NotNull final String userId, @NotNull final Integer index
    ) {
        @Nullable final Optional<E> entity =
                Optional.ofNullable(findOneByIndex(userId, index));
        entity.ifPresent(entities::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeOneByName(
            @NotNull final String userId, @NotNull final String name
    ) {
        @Nullable final Optional<E> entity
                = Optional.ofNullable(findOneByName(userId, name));
        entity.ifPresent(entities::remove);
        return entity.orElse(null);
    }

}
