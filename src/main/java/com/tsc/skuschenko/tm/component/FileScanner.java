package com.tsc.skuschenko.tm.component;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.bootstrap.Bootstrap;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private static final String FILES_PATH = "./";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService =
            Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void init() {
        @Nullable final IPropertyService propertyService =
                bootstrap.getPropertyService();
        final int timeout = propertyService.getFileScannerTime();
        executorService.scheduleWithFixedDelay(
                this, 0, timeout, TimeUnit.SECONDS
        );
    }

    @SneakyThrows
    @Override
    public void run() {
        @NotNull final File files = new File(FILES_PATH);
        @NotNull final Collection<AbstractCommand> commands =
                bootstrap.getCommandService().getArgs();
        Arrays.stream(files.listFiles()).filter(File::isFile)
                .forEach(file -> commands.stream().filter(command ->
                        command.name().equals(file.getName())
                ).forEach(command -> {
                    bootstrap.parseCommand(file.getName());
                    file.delete();
                }));
    }

}
