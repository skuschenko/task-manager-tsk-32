package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand abstractCommand);

    @NotNull
    Collection<AbstractCommand> getArgs();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull
    AbstractCommand getCommandByArg(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<AbstractCommand> getCommands();

}